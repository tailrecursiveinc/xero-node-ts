declare module 'xero-node' {
  interface XeroTypes {
    userAgent: string
    redirectOnError: boolean
    consumerKey?: string
    consumerSecret?: string
    authorizeCallbackUrl?: string
  }

  export class PublicApplication extends RequireAuthorizationApplication {
    constructor(config: XeroTypes)
  }

  class RequireAuthorizationApplication {
    getRequestToken: (
      extras: (err: Error, token: string, secret: string) => void
    ) => void
    buildAuthorizeUrl: (token: string, object: object) => string
    setAccessToken: (token: string, secret: string, verifier: string) => string
    core: Core
  }

  interface Core {
    accounts: Accounts
    bankTransactions: BankTransactions
  }

  interface Accounts {
    getAccounts: (options: object) => Account[]
  }

  interface Account {
    Code: string
    Name: string
    Type: string
    BankAccountNumber: string
    AccountID: string
    Status?: string
    Description?: string
    BankAccountType?: string
    CurrencyCode?: string
    TaxType?: string
    EnablePaymentsToAccount?: boolean
    ShowInExpenseClaims?: boolean
    Class?: string
    SystemAccount?: string
    ReportingCode?: string
    ReportingCodeName?: string
    HasAttachments?: boolean
    UpdatedDateUTC?: Date
  }

  interface BankTransactions {
    newBankTransaction: (bankTransaction: BankTransaction) => BankTransaction
    saveBankTransactions: (
      bankTransaction: BankTransaction[]
    ) => SaveBankTransactionResponse
  }

  interface SaveBankTransactionResponse {
    entities: BankTransaction[]
  }

  interface BankTransaction {
    BankTransactionID: string
    Status: string
    Type?: string
    Contact?: any
    LineItems?: any
    BankAccount?: BankAccount
    IsReconciled?: boolean
    Date?: Date
    Reference?: string
    CurrencyCode?: string
    CurrencyRate?: string
    Url?: string
    LineAmountTypes?: string
    SubTotal?: number
    TotalTax?: number
    Total?: number
    PrepaymentID?: string
    OverpaymentID?: string
    UpdatedDateUTC?: Date
    HasAttachments?: boolean
    FullyPaidOnDate?: Date
  }

  interface BankAccount {
    AccountID: string
  }
}
